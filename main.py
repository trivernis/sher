#Alpha 1.0
import os, engine, optparse, math, subprocess, rollbar

database_file = 'database.db'
last_results = []
bookOptions = True
rollbar.init(key,environment=environment)

def init():
    global se
    se = engine.SearchEngine(load_database())

def createDatabase(path):
    files = []
    database_output = open(input("Output-File Path: "),"w")
    for (pa, dirnames, filenames) in os.walk(path):
        files.extend(os.path.join(pa, name) for name in filenames)
    print('found {} files'.format(len(files)))

    for file in files:
        try:
            database_output.write(file+'\n')
        except Exception as er:
            print(er)
    print('wrote Output-File')

def load_database():
    file = open(database_file,'r')
    return [line.strip('\r\n') for line in file.readlines()]

def singleSearch(query, n=10):
    init()
    results = se.search(query)
    printResults(results, query,n=n)

def printResults(results,query ,n=10,s=0):
    print("\n\n---- {} results for >>{}<<\n".format(len(results), query))
    count = s

    for result in results[s:s+n]:
        print("[{}]  {}\n".format(count,result))
        count += 1

    print("---- Site {} of {}".format(math.ceil(s/n)+1, math.ceil(len(results)/n)))

def process_input():
    query=input("Search: ")

    if '/s' in query:
        site=int(query.strip('/s'))
        printResults(last_results, "Site {}".format(site), s=((site-1)*10))

    elif '/f' in query:
        position=int(query.strip('/f'))
        subprocess.Popen(r'explorer /select,"{}"'.format(last_results[position]))

    elif advancedQuery(query):
        pass

    else:
        last_results.clear()
        results = se.search(query)
        last_results.extend(results)
        printResults(results, query)

def advancedQuery(query):
    if bookOptions:
        if 'author: ' in query.lower():
            last_results.clear()
            query = query.replace('author: ','')
            qsplit = query.split(' ')
            query = '"{n} {s}" "{s}, {n}"'.format(n=qsplit[0],s=qsplit[-1])
            results = se.search(query)
            last_results.extend(results)
            printResults(results, query)
            return True


def search_routine():
    while 1:
        try:
            process_input()
        except Exception as error:
            print("ERROR {}".format(error))
            print("\n Errors can occure because this program is still in early stage of developement")

if __name__ == '__main__':
    optionParser = optparse.OptionParser('usage%prog '+'start the program')
    optionParser.add_option('-q', dest='query', type='string', help='a search request')
    optionParser.add_option('-d', dest='database', type='string', help='the file containing the database')
    optionParser.add_option('-r', dest="refresh" ,type='string', help='refreshes/builds the database')
    optionParser.add_option('-n', dest='num' , type='int', help='the amout of results for single search')
    (options, args) = optionParser.parse_args()

    if options.database:
        database_file = options.database

    if options.query and not options.refresh:
        if options.num:
            singleSearch(options.query, n = options.num)
        else:
            singleSearch(options.query)
    elif options.refresh:
        createDatabase(options.refresh)
    else:
        init()
        search_routine()
